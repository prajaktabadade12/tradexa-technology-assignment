import { Component, OnInit } from '@angular/core';
import {FilterService} from '../filter.service'
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  searchInput:string;
  selectedName:string;
  

  constructor(private Service: FilterService) { }

  ngOnInit(): void {
    this.Service.selectedName.subscribe((name: { name: string; })=>{
      this.selectedName= name.name;
    })
  }

  search(){
    this.Service.filterData(this.searchInput);
  }

}
