import { Component, OnInit } from '@angular/core';
import { getMaxListeners } from 'process';
import {FilterService} from '../filter.service'
import {Subscription} from 'rxjs'
import {userList, arrayType} from '../list-model'

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  list:Array<arrayType>=[];
  subscription:Subscription;
  constructor(private Service: FilterService) {
    this.list=Service.getList();
    // this.list= Service.filteredList.subscribe(list=>{
    //   console.log(Service.getList());
    // })
   }

  ngOnInit(): void {
    // this.list = this.Service.getList()
    //   console.log(this.list);
     this.Service.filteredList.subscribe((list: arrayType[])=>{
      console.log(list);
      this.list=list;
    })
    // console.log(this.list,'updated');
}
selectedItem(value){
this.Service.getSelectedName(value);
}
}