import { isNgTemplate } from '@angular/compiler';
import { Injectable, IterableDiffers } from '@angular/core';
import { getMaxListeners } from 'process';
import {userList, arrayType} from './list-model';
import {EventEmitter, Output} from '@angular/core'
@Injectable({
  providedIn: 'root'
})
export class FilterService {
   list:Array<arrayType>=[];
   filteredArray:Array<arrayType>=[];
  @Output() filteredList = new EventEmitter();
  @Output() selectedName = new EventEmitter();
  constructor() {
    this.list = userList;
    
   }
   getList(){
    return this.list
}
filterData(searchString:string){
  // console.log(searchString);
  if(searchString!=''){
    
    this.filteredArray=this.list.filter(item=>{
      return item.name.includes(searchString)
    })
    this.filteredList.emit(this.filteredArray);
    // this.getList()
    console.log(this.filteredArray,'if');
  }else{
    this.list=userList;
    // this.getList()
    this.filteredList.emit(this.list);
    console.log(this.list,'else');
  }
  }
  getSelectedName(value){
    this.selectedName.emit(value);
  }
}
